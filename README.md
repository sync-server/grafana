### Grafana and Prometheus in docker-compose on jwilder nginx-proxy
[I use this VPS hosting](https://www.hostens.com/?affid=1251)

### Requirements
1. This installation is relay on configured [jwilder/nginx-proxy](https://github.com/nginx-proxy/nginx-proxy).

    I use this installation of [nginx-proxy](https://gitlab.com/sync-server/nginx-proxy)

1. Domain which points to your server.

1. In `docker-compose.yml` file setup `BASE_DOMAIN` variable in `your gitlab project - Settings - CI/CD - Variables` if you use CI/CD or add it to `.env` file near `docker-compose.yml`

## Prometheus
1. Setup proper URL for Grafana in `docker-compose.yml`
2. Close the access to prometheus domain with Nginx and basic auth.
https://prometheus.io/docs/guides/basic-auth/

## Prometheus alerts
Check `prometheus/alert.rules` file

## Telegram alerts
Configure `TELEG_ADMIN` and `TELEG_TOKEN` variables in `docker-compose.yml`.

To get this values check this [documentation](https://github.com/metalmatze/alertmanager-bot#configuration)

Add variables to `your gitlab project - Settings - CI/CD - Variables` if you use CI/CD or add it to `.env` file near `docker-compose.yml`

## Grafana
1. Setup variable `GRAFANA_ADMIN_PASS` in your GitLab CI/CD or in `grafana/config.monitoring` if you don't use CI/CD.
2. Setup proper URL for Grafana in `docker-compose.yml`

## Grafana dashboards
- Docker monitoring
![Dashboard for docker](docker-dashboard.png)
- mdadm (RAID monitoring)
![Dashboard for mdadm](mdadm-dashboard.png)

Check `grafana/provisioning/dashboards` folder

## Note for CI/CD
This variables are set up in GitLab CI/CD variables:
```
BASE_DOMAIN
GRAFANA_ADMIN_PASS
TELEG_ADMIN
TELEG_TOKEN
```